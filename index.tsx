import { App } from "./src/App";
import "./index.css";
import React from "react";
import { createRoot } from "react-dom/client";
import "normalize.css";

import { Provider } from "react-redux";
import { store } from "./src/redux/store";
const rootElement = document.getElementById("app");
if (!rootElement) throw new Error("Failed to find the root element");
const root = createRoot(rootElement);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);
