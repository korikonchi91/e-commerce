import path from "path";
import MiniCssExtractPluging from "mini-css-extract-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
const ROOT_DIR = path.resolve(__dirname);
const SRC_DIR = `${ROOT_DIR}/src`;
const ROOT_DIST = `${ROOT_DIR}/build`;

const configWebpack = (env: any, arg: any) => {
  const { mode } = arg;
  const productionMode = mode === "production";

  return {
    entry: `${ROOT_DIR}/index.tsx`,
    output: {
      path: ROOT_DIST,
      filename: productionMode ? "[name].[contenthash].js" : "bundle.js",
      clean: true,
    },

    devtool: !productionMode && "source-map",
    resolve: {
      alias: {
        nested: `${SRC_DIR}/components/nested1`,
      },
      extensions: [".ts", ".tsx", ".js", ".jsx"],
    },
    setupMiddlewares: () => {},
    module: {
      rules: [
        {
          test: [/\.tsx$/, /\.jsx$/, /\.ts$/, /\.js$/],
          exclude: "/node_modules/",

          use: {
            loader: "babel-loader",
            options: {
              presets: [
                "@babel/preset-env",
                "@babel/preset-react",
                "@babel/preset-typescript",
              ],
            },
          },
        },

        {
          test: [/\.html$/],
          use: "html-loader",
          exclude: "/node_modules/",
        },
        {
          test: [/\.s[ac]ss$/i],
          use: [
            { loader: "style-loader" },
            { loader: "css-modules-typescript-loader" },
            { loader: "css-loader", options: { modules: true } }, // to convert the resulting CSS to Javascript to be bundled
            { loader: "sass-loader" },
          ],
        },

        {
          test: /\.css$/,
          use: [{ loader: "style-loader" }, { loader: "css-loader" }],
        },
        {
          test: /\.svg$/,
          use: "file-loader",
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: "./index.html",
        template: `${ROOT_DIR}/public/index.html`,
        inject: true,
        minify: false,
      }),
      new MiniCssExtractPluging({
        filename: "main.css",
      }),
    ],
  };
};
export default configWebpack;
