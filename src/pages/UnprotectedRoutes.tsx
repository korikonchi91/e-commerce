import React from "react";
import { useSelector } from "react-redux";
import { Outlet, useNavigate } from "react-router-dom";
import { AppState } from "src/redux/reducers";
import { useDispatcher } from "../redux/hooks/useDispatch";
import { Loading } from "../components/Loading";

const UnProtectedRoute = () => {
  const { initApp } = useDispatcher();
  const navigate = useNavigate();
  const { isAuthorized } = useSelector((state: AppState) => state.app.session);
  React.useEffect(() => {
    initApp();
  }, []);

  //mount component isAuthorized === false , initapp change isAuthorized === true dependencies useEffect navigate to "/"
  React.useEffect(() => {
    if (isAuthorized) {
      void navigate("/", { replace: true });
    }
  }, [isAuthorized]);

  return !isAuthorized ? <Outlet /> : <Loading />;
};
export default UnProtectedRoute;
