import React from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { AppState } from "../redux/reducers";
import { useDispatcher } from "../redux/hooks/useDispatch";
import { Loading } from "../components/Loading";

type Props = {
  children: JSX.Element;
};

export const ProtectedRoute = ({ children }: Props) => {
  const { initApp } = useDispatcher();
  const navigate = useNavigate();
  const { isAuthorized } = useSelector((state: AppState) => state.app.session);

  React.useEffect(() => {
    initApp();
  }, []);

  React.useEffect(() => {
    if (!isAuthorized) {
      void navigate("/register", { replace: true });
    }
  }, [isAuthorized]);

  return isAuthorized ? children : <Loading />;
};
