import React from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { Loading } from "../../components/Loading";
import { useDispatcher } from "../../redux/hooks/useDispatch";
import { useSelector } from "../../redux/hooks/useSelector";

export const ProtectedAdmin = () => {
  const { initApp } = useDispatcher();
  const navigate = useNavigate();
  const { isAdmin } = useSelector((state) => state.app.session);

  React.useEffect(() => {
    initApp();
  }, []);

  React.useEffect(() => {
    if (!isAdmin) {
      void navigate("/", { replace: true });
    }
  }, [isAdmin]);

  return isAdmin ? <Outlet /> : <Loading />;
};
