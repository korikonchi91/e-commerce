import { ProtectedRoute } from "./ProtectedRoute";
import UnProtectedRoute from "./UnprotectedRoutes";
import Error from "./Error";
import { Landing } from "./Landing";
import { Register } from "./Register";
import { ReduxApp } from "./ReduxApp";
import { ProtectedAdmin } from "./admin/ProtectedAdmin";
import { Admin } from "./admin/Admin";
export {
  ProtectedRoute,
  UnProtectedRoute,
  Error,
  Landing,
  Register,
  ReduxApp,
  ProtectedAdmin,
  Admin,
};
