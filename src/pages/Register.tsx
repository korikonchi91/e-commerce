import React from "react";
import { useState, useEffect } from "react";
// import { Logo, FormRow } from "../components";
// import Wrapper from "../assets/wrappers/RegisterPage";
// import { toast } from "react-toastify";

// import { loginUser, registerUser } from "../features/user/userSlice";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { FormRow } from "../components";
import { AppState } from "../redux/reducers";
import { AlertStatus } from "../redux/types/userAppTypes";
import { useDispatcher } from "../redux/hooks/useDispatch";
import { useSelector } from "../redux/hooks/useSelector";

const Wrapper = styled.section`
  display: grid;
  align-items: center;
  .logo {
    display: block;
    margin: 0 auto;
    margin-bottom: 1.38rem;
  }
  .form {
    max-width: 400px;
    border-top: 5px solid var(--primary-500);
  }

  h3 {
    text-align: center;
  }
  p {
    margin: 0;
    margin-top: 1rem;
    text-align: center;
  }
  .btn {
    margin-top: 1rem;
  }
  .member-btn {
    background: transparent;
    border: transparent;
    color: var(--primary-500);
    cursor: pointer;
    letter-spacing: var(--letterSpacing);
  }
`;

export default Wrapper;

const INITIAL_STATE = {
  name: "",
  email: "",
  password: "",
  isMember: true,
};

export const Register = () => {
  const [values, setValues] = useState(INITIAL_STATE);
  const { status } = useSelector((state: AppState) => state.app.alert);

  const { logInSession } = useDispatcher();
  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const name = e.target.name;
    const value = e.target.value;

    setValues({ ...values, [name]: value });
  };
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { name, email, password, isMember } = values;
    if (!email || !password || (!isMember && !name)) {
      //*login or register value inputs
      // toast.error("Please fill out all fields");
      return;
    }
    // if (isMember) {
    //   dispatch(loginUser({ email: email, password: password }));
    //   return;
    // }
    // dispatch(registerUser({ name, email, password }));
  };

  const toggleMember = () => {
    setValues({ ...values, isMember: !values.isMember });
  };

  const handleClickLogin = () => {
    logInSession();
  };
  return (
    <Wrapper className="full-page">
      <form className="form" onSubmit={onSubmit}>
        <h3>{values.isMember ? "Login" : "Register"}</h3>
        {/* name field */}
        {!values.isMember && (
          <FormRow
            type="text"
            name="name"
            value={values.name}
            handleChange={handleChange}
          />
        )}
        {/* email field */}
        <FormRow
          type="email"
          name="email"
          value={values.email}
          handleChange={handleChange}
        />
        {/* password field */}
        <FormRow
          type="password"
          name="password"
          value={values.password}
          handleChange={handleChange}
        />
        <button
          type="submit"
          className="btn btn-block"
          disabled={status === AlertStatus.LOADING}
          onClick={handleClickLogin}
        >
          {status === AlertStatus.LOADING ? "loading..." : "submit"}
        </button>

        <p>
          {values.isMember ? "Not a member yet?" : "Already a member?"}
          <button type="button" onClick={toggleMember} className="member-btn">
            {values.isMember ? "Register" : "Login"}
          </button>
        </p>
      </form>
    </Wrapper>
  );
};
