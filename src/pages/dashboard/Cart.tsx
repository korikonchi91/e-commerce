import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/reducers";
import { counterReduce } from "../../utils/counterReduce";
import { ProductCart } from "../../components/ProductCart";
import { useDispatcher } from "../../redux/hooks/useDispatch";
import { Product } from "../../redux/types/userAppTypes";
export const Cart = () => {
  const { session } = useSelector((state: AppState) => state.app);
  const products = session.products;
  const cart = session.myProfile.cart;
  const [cartState, setCartState] = React.useState(cart);
  const counterCart = counterReduce(cartState);
  const { UpdateProducts } = useDispatcher();
  const entriesCart = Object.entries(counterCart);
  React.useEffect(() => {
    UpdateProducts(cartState, "cart");
  }, [cartState]);

  const addCart = (idCartArray: Array<number> | undefined) => {
    setCartState((prevState) => [...prevState, ...(idCartArray || [])]);
  };

  const removeCart = (indexRemove: number | undefined) => {
    cartState.splice(indexRemove || 0, 1);
    const newCart = cartState;
    setCartState([...newCart]);
  };

  return (
    <div>
      {entriesCart.map(([key, property]) => {
        const Product = products.find((product) => product.id === Number(key));
        const productsCart = cartState.filter(
          (id) => id === Product?.id
        ).length;
        return Product ? (
          <ProductCart
            key={Product?.id}
            Product={Product}
            property={property}
            cartLength={productsCart}
            idCartArray={[Product?.id]}
            setCartState={setCartState}
            cartState={cartState}
            indexRemove={cartState.indexOf(Product.id)}
            addCart={addCart}
            removeCart={removeCart}
          />
        ) : (
          "not cart products"
        );
      })}
    </div>
  );
};
