import styled from "styled-components";
import React from "react";
import { Outlet, useNavigate } from "react-router-dom";
import NavBar from "../../components/NavBar";
const Wrapper = styled.section`
  .dashboard {
    display: grid;
    grid-template-columns: 1fr;
    .action {
      max-width: 200px;
    }
  }
  .dashboard-page {
    width: 90vw;
    margin: 0 auto;
    padding: 2rem 0;
  }
`;

const SharedLayout = () => {
  const navigate = useNavigate();
  const prev = () => {
    navigate(-1);
  };
  const next = () => {
    navigate(-1);
  };
  return (
    <Wrapper>
      <main className="dashboard">
        <NavBar />
        <button className="action" onClick={prev}>
          atras
        </button>
        <button className="action" onClick={next}>
          delante
        </button>
        <div>
          <div className="dashboard-page">
            <Outlet />
          </div>
        </div>
      </main>
    </Wrapper>
  );
};
export default SharedLayout;
