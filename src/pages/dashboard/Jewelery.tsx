import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "src/redux/reducers";
import { Product } from "../../redux/types/userAppTypes";
import { ProductCard } from "../../components/ProductCard";

export const Jewelery = () => {
  const { jewelery } = useSelector(
    (state: AppState) => state.app.session.categories
  );
  return (
    <div>
      <ul>
        {jewelery.map((product: Product) => {
          return <ProductCard product={product} />;
        })}
      </ul>
    </div>
  );
};
