import React from "react";

import { useSearchParams, useLocation, useParams } from "react-router-dom";
import { searchData } from "../../utils/searchData";
import { CardProduct } from "../../components/CardProduct";
import { searchGeneric } from "c:/Users/korik/OneDrive/Desktop/webpack/src/utils/searchGeneric";
export const Search = () => {
  const location = useLocation();
  const paramsUse = useParams();

  const [params, setParams] = useSearchParams();
  const onChangeQuery = (e: React.ChangeEvent<HTMLInputElement>) => {
    setParams({ search: e.target.value });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log("submit");
  };

  return (
    <div>
      <h2>introduce busqueda</h2>
      <form onSubmit={handleSubmit}>
        <input
          value={params.get("search") || ""}
          type="text"
          onChange={onChangeQuery}
        />
        <button type="submit"> submit</button>
      </form>
      <div>
        {searchData
          .filter((product) =>
            searchGeneric(product, ["product"], params.get("search") || "")
          )
          .map((product) => {
            return <CardProduct key={product.id} product={product} />;
          })}
      </div>
    </div>
  );
};
