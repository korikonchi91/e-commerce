import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/reducers";
import { Product } from "../../redux/types/userAppTypes";
import { ProductCard } from "../../components/ProductCard";

export const Electronics = () => {
  const { electronics } = useSelector(
    (state: AppState) => state.app.session.categories
  );
  return (
    <div>
      <ul>
        {electronics.map((product: Product) => {
          return <ProductCard product={product} />;
        })}
      </ul>
    </div>
  );
};
