import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { AppState } from "../../redux/types/AppTypes";
import { Product } from "../../redux/types/userAppTypes";
import styled from "styled-components";
import { useDispatcher } from "../../redux/hooks/useDispatch";

const Wrapper = styled.div`
  .container {
    display: flex;
    button {
      background: none;
      border: none;
      cursor: pointer;

      svg {
        fill: red;
      }
    }
  }

  .number-input {
    display: inline-flex;
    button:last-child {
      margin-left: 1rem;
      width: 4rem;
    }
  }

  .number-input,
  .number-input * {
    box-sizing: border-box;
  }

  .number-input button {
    outline: none;
    -webkit-appearance: none;
    background-color: transparent;
    border: 2px solid #ddd;

    align-items: center;
    justify-content: center;
    width: 3rem;
    height: 3rem;
    cursor: pointer;
    margin: 0;
    position: relative;
  }

  .number-input input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    font-family: sans-serif;
    max-width: 5rem;
    padding: 0.5rem;
    border: solid #ddd;
    border-width: 0 2px;
    font-size: 2rem;
    height: 3rem;
    font-weight: bold;
    text-align: center;
  }

  .presentation {
    display: flex;
    flex-direction: column;
    flex-basis: 70%;
    justify-content: center;
    align-items: center;
  }
  .information {
    display: flex;
    flex-direction: column;
    flex-basis: 30%;
    justify-content: center;
    align-items: center;
    span {
      margin-bottom: 2rem;
    }
  }
  figure {
    height: 600px;
    width: 600px;
    overflow: hidden;
    img {
      object-fit: fill;
      width: 100%;
      height: 100%;
      :hover {
        transform: scale(1.2);
        transition-property: transform;
        transition-duration: 700ms;
        transition-timing-function: ease-in;
        transition-delay: 0s;
      }
    }
  }
`;

export const SingleProduct: React.FC = () => {
  const { addCartProduct } = useDispatcher();
  const { id } = useParams();

  const { session } = useSelector((state: AppState) => state.app);
  const products = session.products;

  const [counterCart, setcounterCart] = React.useState<number>(1);
  const product = products.find(
    (product: Product) => product.id === Number(id)
  );
  const add = () => {
    setcounterCart((PrevState) => PrevState + 1);
  };
  const minus = () => {
    if (counterCart < 2) return;

    setcounterCart((PrevState) => PrevState - 1);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setcounterCart(Number(e.target.value));
  };
  const handleClickCart = () => {
    const arrayCart = Array(counterCart).fill(Number(id));
    addCartProduct(arrayCart, session);
  };
  const { image, category, description, price } = product;
  return (
    <Wrapper>
      <div className="container">
        <div className="presentation">
          <h2> {category}</h2>
          <figure>
            <img src={image} alt="image" />
          </figure>
        </div>
        <div className="information">
          <span>{description}</span>
          <h3>{price}</h3>
          <div className="number-input">
            <button onClick={minus} className="minus">
              -
            </button>
            <input type="number" value={counterCart} onChange={handleChange} />
            <button onClick={add} className="plus">
              +
            </button>

            <button className="addCart" onClick={handleClickCart}>
              add cart
            </button>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};
