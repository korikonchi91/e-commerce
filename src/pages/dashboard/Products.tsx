import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "src/redux/reducers";
import { Product } from "../../redux/types/userAppTypes";
import { ProductCard } from "../../components/ProductCard";

export const Products = () => {
  const { products } = useSelector((state: AppState) => state.app.session);
  return (
    <div>
      <ul>
        {products.map((product: Product) => {
          return <ProductCard product={product} />;
        })}
      </ul>
    </div>
  );
};
