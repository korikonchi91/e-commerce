import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/reducers";
import { ProductCard } from "../../components/ProductCard";
import { Product } from "../../redux/types/userAppTypes";

export const Women = () => {
  const { women } = useSelector(
    (state: AppState) => state.app.session.categories
  );
  return (
    <div>
      <ul>
        {women.map((product: Product) => {
          return <ProductCard product={product} />;
        })}
      </ul>
    </div>
  );
};
