import { Child } from "./Child";
import { Men } from "./Men";
import { Products } from "./Products";
import { Jewelery } from "./Jewelery";
import { Search } from "./Search";
import SharedLayout from "./SharedLayout";
import { SingleProduct } from "./SingleProduct";
import { SharedProductLayout } from "./SharedLayoutProducts";
import { Women } from "./Women";
import { Electronics } from "./Electronics";
import { Cart } from "./Cart";
import { WishList } from "./WishList";
export {
  Child,
  Men,
  Products,
  Jewelery,
  Search,
  SharedLayout,
  SingleProduct,
  SharedProductLayout,
  Women,
  Electronics,
  Cart,
  WishList,
};
