import React from "react";
import { Link } from "react-router-dom";
import { getProducts } from "../redux/actions/ActionCreatorApp";
import { useDispatcher } from "../redux/hooks/useDispatch";

export const Landing = () => {
  const { getProducts } = useDispatcher();

  React.useEffect(() => {
    getProducts();
  }, []);
  return (
    <div>
      <Link to={"/register"}>nav register</Link>
    </div>
  );
};
