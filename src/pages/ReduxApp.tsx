import React from "react";
import { useDispatch } from "react-redux";
import { AppState } from "../redux/reducers";
import { useSelector } from "../redux/hooks/useSelector";

export const ReduxApp = () => {
  const { name } = useSelector((state: AppState) => state.name);
  const dispatch = useDispatch();

  const handleSetName = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch({ type: "SET_NAME", payload: e.target.value });
  };
  return (
    <div>
      <div>
        <input type="text" onChange={handleSetName} />
        {name}
      </div>
    </div>
  );
};
