import { NavLink, useLocation } from "react-router-dom";
import React from "react";
import { linksNav } from "../utils/objectsData";
import { useSelector } from "../redux/hooks/useSelector";

export const NavLinks = () => {
  const { cart } = useSelector((state) => state.app.session.myProfile);
  return (
    <div className="nav-links">
      {linksNav.map((link) => {
        const { text, path, id, icon } = link;
        const isCart = text === "Cart";

        return (
          <NavLink
            to={path}
            className={({ isActive }) => {
              return isActive ? "nav-link active" : "nav-link";
            }}
            key={id}
          >
            {isCart && cart.length > 0 && (
              <div className="number">
                <span>{cart.length}</span>
              </div>
            )}
            <div className="icon"> {icon}</div>
            {text}
          </NavLink>
        );
      })}
    </div>
  );
};
