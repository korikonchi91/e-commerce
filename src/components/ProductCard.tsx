import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Product } from "../redux/types/userAppTypes";
import { GiTentacleHeart } from "react-icons/gi";
import { useDispatcher } from "../redux/hooks/useDispatch";
import { useSelector } from "../redux/hooks/useSelector";
interface Props {
  product: Product;
}
const Wrapper = styled.div`
  .container {
    display: flex;
    button {
      background: none;
      border: none;
      cursor: pointer;

      svg {
        fill: red;
      }
    }
  }

  .presentation {
    display: flex;
    flex-direction: column;
    flex-basis: 70%;
    justify-content: center;
    align-items: center;
  }
  .information {
    display: flex;
    flex-direction: column;
    flex-basis: 30%;
    justify-content: center;
    align-items: center;
    span {
      margin-bottom: 2rem;
    }
  }
  figure {
    height: 600px;
    width: 600px;
    overflow: hidden;
    img {
      object-fit: fill;
      width: 100%;
      height: 100%;
      :hover {
        transform: scale(1.2);
        transition-property: transform;
        transition-duration: 700ms;
        transition-timing-function: ease-in;
        transition-delay: 0s;
      }
    }
  }
`;
export const ProductCard: React.FC<Props> = ({ product }) => {
  const { addCartProduct, addWishList } = useDispatcher();

  const handleClickCart = () => {
    addCartProduct([product.id]);
  };

  const handleClickWish = () => {
    addWishList([product.id]);
  };

  const { image, price, title } = product;
  return (
    <Wrapper>
      <div className="container">
        <div className="presentation">
          <h2> {title}</h2>
          <figure>
            <img src={image} alt="image" />
          </figure>
        </div>
        <div className="information">
          <h3>{price}</h3>
          <Link to={`/products/${product.id}`}>see product</Link>
          <button onClick={handleClickCart}>add Cart</button>
          <button onClick={handleClickWish}>
            <GiTentacleHeart />
          </button>
        </div>
      </div>
    </Wrapper>
  );
};
