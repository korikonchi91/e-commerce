import styled from "styled-components";
import { NavLinks } from "./NavLinks";
import React from "react";
import { useDispatcher } from "../redux/hooks/useDispatch";

const Wrapper = styled.aside`
  display: block;
  box-shadow: 1px 0px 0px 0px rgba(0, 0, 0, 0.1);
  .navbar-container {
    background: var(--white);
    width: 100%;
    height: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .content {
    position: sticky;
    top: 0;
  }

  header {
    height: 6rem;
    display: flex;
    align-items: center;
    padding-left: 2.5rem;
  }
  .nav-links {
    display: flex;
  }
  .number {
    display: flex;
    justify-content: center;
    align-items: center;
    top: 20%;
    left: 30%;
    z-index: -1;
    width: 20px;
    height: 20px;
    background-color: red;
    border-radius: 50%;
    position: absolute;
    color: white;
  }
  .nav-link {
    position: relative;
    display: flex;
    align-items: center;
    color: var(--grey-500);
    padding: 1rem;
    text-transform: capitalize;
    transition: var(--transition);
  }
  .nav-link:hover {
    background: var(--grey-50);
    padding-left: 2rem;
    color: var(--grey-900);
  }
  .nav-link:hover .icon {
    color: var(--primary-500);
  }
  .icon {
    font-size: 1.5rem;
    margin-right: 1rem;
    display: grid;
    place-items: center;
    transition: var(--transition);
  }
  .active {
    color: var(--grey-900);
  }
  .active .icon {
    color: var(--primary-500);
  }
`;

export const NavBar = () => {
  const { logoutSession } = useDispatcher();
  const handleLogout = () => {
    logoutSession();
  };
  return (
    <Wrapper>
      <div className={"navbar-container show-sidebar "}>
        <div className="content">
          <NavLinks />
        </div>
        <button className="btn" onClick={handleLogout}>
          cerrar sesion
        </button>
      </div>
    </Wrapper>
  );
};
export default NavBar;
