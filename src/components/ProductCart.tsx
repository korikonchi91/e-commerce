import React from "react";
import styled from "styled-components";
import { Product } from "../redux/types/userAppTypes";

const Wrapper = styled.div`
  .container {
    display: grid;
    align-items: center;
    justify-content: start;
  }
  input {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
  }
  figure {
    height: 300px;
    width: 300px;
    overflow: hidden;
    img {
      object-fit: fill;
      width: 100%;
      height: 100%;
    }
  }
`;
interface Props {
  Product?: Product;
  property: number;
  cartLength: number;
  idCartArray?: Array<number>;
  setCartState: React.Dispatch<React.SetStateAction<number[]>>;
  indexRemove: number | undefined;
  cartState: Array<number>;
  addCart: (idCartArray: Array<number> | undefined) => void;
  removeCart: (indexRemove: number | undefined) => void;
}
export const ProductCart: React.FC<Props> = ({
  Product,
  property,
  cartLength,
  idCartArray,
  indexRemove,
  addCart,
  removeCart,
}) => {
  const add = () => {
    return addCart(idCartArray);
  };

  const minus = () => {
    return removeCart(indexRemove);
  };
  return (
    <Wrapper>
      <div className="container">
        <figure>
          <img src={Product?.image} />
        </figure>
        <span> price: {Product?.price} $ </span> <br />
        <span> amount: {property}</span>
        <button onClick={add}>+</button>
        <input type="number" value={cartLength} />
        <button onClick={minus}>-</button>
      </div>
    </Wrapper>
  );
};
