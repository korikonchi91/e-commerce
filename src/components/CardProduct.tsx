import React from "react";
import { Link } from "react-router-dom";
import { Data } from "src/utils/searchData";
import styled from "styled-components";
const Wrapper = styled.div`
  div {
    padding: 1rem;
    display: flex;
    flex-direction: column;
    img {
      height: 100px;
      width: 100px;
    }
  }
`;
interface Props {
  product: Data;
}

export const CardProduct: React.FC<Props> = ({ product }) => {
  return (
    <Wrapper key={product.id}>
      <div>
        <h2>{product.product}</h2>
        <img src={product.image} alt="image" />
        <Link to={`/products/${product.product}`}>nav single product</Link>
      </div>
    </Wrapper>
  );
};
