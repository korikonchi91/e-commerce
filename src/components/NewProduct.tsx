import React from "react";
import { useState } from "react";
import styled from "styled-components";
import { FormRow } from "../components";
import { AlertStatus, Product } from "../redux/types/userAppTypes";
import { useDispatcher } from "../redux/hooks/useDispatch";
import { useSelector } from "../redux/hooks/useSelector";
import { SomeEmpty } from "../utils/counterReduce";
import { toast } from "react-toastify";
import { inputValues } from "../utils/objectsData";

const Wrapper = styled.section`
  display: grid;
  align-items: center;
  .logo {
    display: block;
    margin: 0 auto;
    margin-bottom: 1.38rem;
  }
  .form {
    max-width: 400px;
    border-top: 5px solid var(--primary-500);
  }

  h3 {
    text-align: center;
  }
  p {
    margin: 0;
    margin-top: 1rem;
    text-align: center;
  }
  .btn {
    margin-top: 1rem;
  }
  .member-btn {
    background: transparent;
    border: transparent;
    color: var(--primary-500);
    cursor: pointer;
    letter-spacing: var(--letterSpacing);
  }
`;

export default Wrapper;

const INITIAL_STATE: Omit<Product, "id"> = {
  title: "",
  category: "",
  description: "",
  image: "",
  price: "",
};

export const NewProduct = () => {
  const [product, setProduct] = useState<Omit<Product, "id">>(INITIAL_STATE);

  const { products } = useSelector((state) => state.app.session);
  const { status } = useSelector((state) => state.app.alert);
  const { appStatus } = useDispatcher();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    setProduct({ ...product, [name]: value });
  };
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const isEmpty = SomeEmpty(Object.values(product));
    isEmpty
      ? appStatus({
          msg: "incomplete credentials",
          status: AlertStatus.ERROR,
        }) && toast.error("incomplete credentials")
      : appStatus({ msg: "", status: AlertStatus.IDLE }) &&
        toast.success("add product");

    const newProduct: Product = {
      ...product,
      id: products.length + 1,
      price: Number(product.price),
    };
    console.log(newProduct);
  };

  return (
    <Wrapper className="full-page">
      <form className="form" onSubmit={onSubmit}>
        <h3>new product</h3>
        {inputValues.map((input) => {
          const { id, type, name } = input;
          return (
            <FormRow
              key={id}
              type={type}
              name={name}
              value={`${product[name as keyof Omit<Product, "id">]}`}
              handleChange={handleChange}
            />
          );
        })}

        <button
          className="btn btn-block"
          disabled={status === AlertStatus.LOADING}
        >
          {status === AlertStatus.LOADING ? "loading..." : "submit"}
        </button>
      </form>
    </Wrapper>
  );
};
