import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import {
  Error,
  Landing,
  ProtectedRoute,
  ReduxApp,
  Register,
  UnProtectedRoute,
  ProtectedAdmin,
  Admin,
} from "./pages";
import { pathApp } from "./utils/Types";
import { WishList } from "./pages/dashboard/WishList";
import { NewProduct } from "./components/NewProduct";
import {
  Men,
  Products,
  Search,
  SharedLayout,
  SingleProduct,
  Jewelery,
  Women,
  Electronics,
  Cart,
} from "./pages/dashboard";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <SharedLayout />
            </ProtectedRoute>
          }
        >
          <Route index element={<Landing />} />

          <Route path={pathApp.MEN} element={<Men />} />
          <Route path={pathApp.WOMEN} element={<Women />} />
          <Route path={pathApp.JEWERELY} element={<Jewelery />} />
          <Route path={pathApp.SEARCH} element={<Search />} />
          <Route path={pathApp.ELECTRONIC} element={<Electronics />} />
          <Route path={pathApp.WISHLIST} element={<WishList />} />
          <Route path={pathApp.CART} element={<Cart />} />
          <Route path={pathApp.REDUX} element={<ReduxApp />} />
          <Route path="products">
            <Route index={true} element={<Products />} />
            <Route path=":id" element={<SingleProduct />} />
          </Route>
        </Route>
        <Route element={<UnProtectedRoute />}>
          <Route path={"register"} element={<Register />} />
        </Route>

        <Route path="admin" element={<ProtectedAdmin />}>
          <Route index element={<Admin />} />
          <Route path={"newproduct"} element={<NewProduct />} />
        </Route>
        <Route path="*" element={<Error />} />
      </Routes>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={true}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </BrowserRouter>
  );
};
