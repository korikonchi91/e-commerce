export const pathApp = {
  PRODUCTS: "products",
  MEN: "men",
  REDUX: "redux",
  WOMEN: "women",
  JEWERELY: "jewelery",
  ELECTRONIC: "electronic",
  SEARCH: "search",
  CART: "CART",
  SINGLE_PRODUCT: ":id",
  WISHLIST: "wishlist",
};
