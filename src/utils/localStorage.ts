import { ObjectSession } from "../redux/types/userAppTypes";
const addUToLocalStorage = (name: string, item: any) => {
  localStorage.setItem(name, JSON.stringify(item));
};

const removeUserFromLocalStorage = (name: string) => {
  localStorage.removeItem(name);
};

const getUserFromLocalStorage = (name: string) => {
  const result = localStorage.getItem(name);
  const user = result ? JSON.parse(result) : null;
  return user;
};

const addNewSessionLocal = (
  session: ObjectSession,
  exp: number,
  name: string,
  newCart: Array<number>
) => {
  addUToLocalStorage("session", {
    ...session,
    exp: exp,
    myProfile: {
      ...session.myProfile,
      [name]: [...newCart],
    },
  });
};

export {
  addNewSessionLocal,
  addUToLocalStorage,
  removeUserFromLocalStorage,
  getUserFromLocalStorage,
};
