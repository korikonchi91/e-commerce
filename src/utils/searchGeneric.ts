export const searchGeneric = <T>(
  object: T,
  keys: Array<keyof T>,
  query: string | number
) => {
  if (query) {
    return keys.some((key): boolean | undefined => {
      const property = object[key];
      if (typeof property === "string" || typeof property === "number") {
        return property
          .toString()
          .toLowerCase()
          .includes(query.toString().toLowerCase());
      } else return false;
    });
  } else return false;
};
