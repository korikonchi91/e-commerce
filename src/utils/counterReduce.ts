export const counterReduce = (reduceArray: Array<string | number>) => {
  const reduce = reduceArray.reduce<Record<string, number>>(
    (acc, product) => (
      acc[product] ? (acc[product] += 1) : (acc[product] = 1), acc
    ),
    {} //acumulator initial values
  );
  return reduce;
};
export const SomeEmpty = (Array: Array<string | number>) => {
  const empty = Array.some(
    (ele) =>
      (typeof ele === "string" && ele === "") ||
      (typeof ele === "number" && ele === 0)
  );
  return empty;
};
