export interface Data {
  id: number;
  product: string;
  image: string;
}
export const searchData: Array<Data> = [
  {
    id: 1,
    product: "Public Utilities",
    image: "http://dummyimage.com/139x100.png/ff4444/ffffff",
  },
  {
    id: 2,
    product: "Finance",
    image: "http://dummyimage.com/182x100.png/cc0000/ffffff",
  },
  {
    id: 3,
    product: "n/a",
    image: "http://dummyimage.com/162x100.png/dddddd/000000",
  },
  {
    id: 4,
    product: "Capital Goods",
    image: "http://dummyimage.com/250x100.png/cc0000/ffffff",
  },
  {
    id: 5,
    product: "Consumer Non-Durables",
    image: "http://dummyimage.com/209x100.png/5fa2dd/ffffff",
  },
  {
    id: 6,
    product: "Capital Goods",
    image: "http://dummyimage.com/171x100.png/dddddd/000000",
  },
  {
    id: 7,
    product: "Health Care",
    image: "http://dummyimage.com/111x100.png/ff4444/ffffff",
  },
  {
    id: 8,
    product: "Finance",
    image: "http://dummyimage.com/205x100.png/5fa2dd/ffffff",
  },
  {
    id: 9,
    product: "Health Care",
    image: "http://dummyimage.com/141x100.png/cc0000/ffffff",
  },
  {
    id: 10,
    product: "Consumer Services",
    image: "http://dummyimage.com/171x100.png/dddddd/000000",
  },
  {
    id: 11,
    product: "Consumer Services",
    image: "http://dummyimage.com/236x100.png/5fa2dd/ffffff",
  },
  {
    id: 12,
    product: "Technology",
    image: "http://dummyimage.com/157x100.png/5fa2dd/ffffff",
  },
  {
    id: 13,
    product: "Basic Industries",
    image: "http://dummyimage.com/211x100.png/dddddd/000000",
  },
  {
    id: 14,
    product: "n/a",
    image: "http://dummyimage.com/141x100.png/cc0000/ffffff",
  },
  {
    id: 15,
    product: "n/a",
    image: "http://dummyimage.com/175x100.png/dddddd/000000",
  },
  {
    id: 16,
    product: "n/a",
    image: "http://dummyimage.com/225x100.png/ff4444/ffffff",
  },
  {
    id: 17,
    product: "Health Care",
    image: "http://dummyimage.com/164x100.png/ff4444/ffffff",
  },
  {
    id: 18,
    product: "n/a",
    image: "http://dummyimage.com/178x100.png/ff4444/ffffff",
  },
  {
    id: 19,
    product: "Energy",
    image: "http://dummyimage.com/125x100.png/dddddd/000000",
  },
  {
    id: 20,
    product: "Energy",
    image: "http://dummyimage.com/111x100.png/dddddd/000000",
  },
  {
    id: 21,
    product: "Miscellaneous",
    image: "http://dummyimage.com/232x100.png/5fa2dd/ffffff",
  },
  {
    id: 22,
    product: "Consumer Services",
    image: "http://dummyimage.com/237x100.png/dddddd/000000",
  },
  {
    id: 23,
    product: "Finance",
    image: "http://dummyimage.com/248x100.png/dddddd/000000",
  },
  {
    id: 24,
    product: "n/a",
    image: "http://dummyimage.com/189x100.png/5fa2dd/ffffff",
  },
  {
    id: 25,
    product: "Finance",
    image: "http://dummyimage.com/108x100.png/cc0000/ffffff",
  },
  {
    id: 26,
    product: "Finance",
    image: "http://dummyimage.com/161x100.png/ff4444/ffffff",
  },
  {
    id: 27,
    product: "Energy",
    image: "http://dummyimage.com/239x100.png/cc0000/ffffff",
  },
  {
    id: 28,
    product: "n/a",
    image: "http://dummyimage.com/208x100.png/dddddd/000000",
  },
  {
    id: 29,
    product: "Consumer Services",
    image: "http://dummyimage.com/242x100.png/cc0000/ffffff",
  },
  {
    id: 30,
    product: "Consumer Services",
    image: "http://dummyimage.com/145x100.png/cc0000/ffffff",
  },
  {
    id: 31,
    product: "Finance",
    image: "http://dummyimage.com/107x100.png/ff4444/ffffff",
  },
  {
    id: 32,
    product: "Technology",
    image: "http://dummyimage.com/121x100.png/cc0000/ffffff",
  },
  {
    id: 33,
    product: "Energy",
    image: "http://dummyimage.com/134x100.png/cc0000/ffffff",
  },
  {
    id: 34,
    product: "n/a",
    image: "http://dummyimage.com/208x100.png/ff4444/ffffff",
  },
  {
    id: 35,
    product: "Finance",
    image: "http://dummyimage.com/172x100.png/dddddd/000000",
  },
  {
    id: 36,
    product: "Finance",
    image: "http://dummyimage.com/180x100.png/dddddd/000000",
  },
  {
    id: 37,
    product: "Finance",
    image: "http://dummyimage.com/198x100.png/ff4444/ffffff",
  },
  {
    id: 38,
    product: "Finance",
    image: "http://dummyimage.com/128x100.png/ff4444/ffffff",
  },
  {
    id: 39,
    product: "Miscellaneous",
    image: "http://dummyimage.com/128x100.png/cc0000/ffffff",
  },
  {
    id: 40,
    product: "Finance",
    image: "http://dummyimage.com/194x100.png/5fa2dd/ffffff",
  },
  {
    id: 41,
    product: "Technology",
    image: "http://dummyimage.com/246x100.png/5fa2dd/ffffff",
  },
  {
    id: 42,
    product: "Public Utilities",
    image: "http://dummyimage.com/222x100.png/5fa2dd/ffffff",
  },
  {
    id: 43,
    product: "Consumer Services",
    image: "http://dummyimage.com/238x100.png/cc0000/ffffff",
  },
  {
    id: 44,
    product: "n/a",
    image: "http://dummyimage.com/150x100.png/cc0000/ffffff",
  },
  {
    id: 45,
    product: "Health Care",
    image: "http://dummyimage.com/142x100.png/ff4444/ffffff",
  },
  {
    id: 46,
    product: "Health Care",
    image: "http://dummyimage.com/168x100.png/dddddd/000000",
  },
  {
    id: 47,
    product: "Miscellaneous",
    image: "http://dummyimage.com/174x100.png/dddddd/000000",
  },
  {
    id: 48,
    product: "Finance",
    image: "http://dummyimage.com/171x100.png/5fa2dd/ffffff",
  },
  {
    id: 49,
    product: "Health Care",
    image: "http://dummyimage.com/121x100.png/dddddd/000000",
  },
  {
    id: 50,
    product: "Technology",
    image: "http://dummyimage.com/204x100.png/dddddd/000000",
  },
  {
    id: 51,
    product: "Energy",
    image: "http://dummyimage.com/214x100.png/ff4444/ffffff",
  },
  {
    id: 52,
    product: "Transportation",
    image: "http://dummyimage.com/207x100.png/cc0000/ffffff",
  },
  {
    id: 53,
    product: "Finance",
    image: "http://dummyimage.com/194x100.png/5fa2dd/ffffff",
  },
  {
    id: 54,
    product: "Finance",
    image: "http://dummyimage.com/140x100.png/ff4444/ffffff",
  },
  {
    id: 55,
    product: "n/a",
    image: "http://dummyimage.com/160x100.png/5fa2dd/ffffff",
  },
  {
    id: 56,
    product: "Consumer Services",
    image: "http://dummyimage.com/153x100.png/dddddd/000000",
  },
  {
    id: 57,
    product: "Consumer Services",
    image: "http://dummyimage.com/198x100.png/cc0000/ffffff",
  },
  {
    id: 58,
    product: "Miscellaneous",
    image: "http://dummyimage.com/228x100.png/ff4444/ffffff",
  },
  {
    id: 59,
    product: "Public Utilities",
    image: "http://dummyimage.com/196x100.png/cc0000/ffffff",
  },
  {
    id: 60,
    product: "n/a",
    image: "http://dummyimage.com/118x100.png/ff4444/ffffff",
  },
  {
    id: 61,
    product: "n/a",
    image: "http://dummyimage.com/180x100.png/ff4444/ffffff",
  },
  {
    id: 62,
    product: "Public Utilities",
    image: "http://dummyimage.com/127x100.png/dddddd/000000",
  },
  {
    id: 63,
    product: "Basic Industries",
    image: "http://dummyimage.com/155x100.png/cc0000/ffffff",
  },
  {
    id: 64,
    product: "Basic Industries",
    image: "http://dummyimage.com/194x100.png/5fa2dd/ffffff",
  },
  {
    id: 65,
    product: "Energy",
    image: "http://dummyimage.com/115x100.png/ff4444/ffffff",
  },
  {
    id: 66,
    product: "Finance",
    image: "http://dummyimage.com/245x100.png/cc0000/ffffff",
  },
  {
    id: 67,
    product: "Consumer Services",
    image: "http://dummyimage.com/244x100.png/cc0000/ffffff",
  },
  {
    id: 68,
    product: "Capital Goods",
    image: "http://dummyimage.com/114x100.png/5fa2dd/ffffff",
  },
  {
    id: 69,
    product: "Consumer Services",
    image: "http://dummyimage.com/183x100.png/dddddd/000000",
  },
  {
    id: 70,
    product: "n/a",
    image: "http://dummyimage.com/176x100.png/5fa2dd/ffffff",
  },
  {
    id: 71,
    product: "Finance",
    image: "http://dummyimage.com/154x100.png/5fa2dd/ffffff",
  },
  {
    id: 72,
    product: "Technology",
    image: "http://dummyimage.com/229x100.png/5fa2dd/ffffff",
  },
  {
    id: 73,
    product: "Consumer Services",
    image: "http://dummyimage.com/179x100.png/cc0000/ffffff",
  },
  {
    id: 74,
    product: "Capital Goods",
    image: "http://dummyimage.com/185x100.png/5fa2dd/ffffff",
  },
  {
    id: 75,
    product: "Consumer Durables",
    image: "http://dummyimage.com/122x100.png/dddddd/000000",
  },
  {
    id: 76,
    product: "Public Utilities",
    image: "http://dummyimage.com/175x100.png/cc0000/ffffff",
  },
  {
    id: 77,
    product: "Basic Industries",
    image: "http://dummyimage.com/232x100.png/dddddd/000000",
  },
  {
    id: 78,
    product: "Consumer Services",
    image: "http://dummyimage.com/243x100.png/ff4444/ffffff",
  },
  {
    id: 79,
    product: "n/a",
    image: "http://dummyimage.com/217x100.png/ff4444/ffffff",
  },
  {
    id: 80,
    product: "Capital Goods",
    image: "http://dummyimage.com/210x100.png/5fa2dd/ffffff",
  },
  {
    id: 81,
    product: "Consumer Services",
    image: "http://dummyimage.com/179x100.png/dddddd/000000",
  },
  {
    id: 82,
    product: "Finance",
    image: "http://dummyimage.com/134x100.png/5fa2dd/ffffff",
  },
  {
    id: 83,
    product: "n/a",
    image: "http://dummyimage.com/111x100.png/5fa2dd/ffffff",
  },
  {
    id: 84,
    product: "Health Care",
    image: "http://dummyimage.com/111x100.png/cc0000/ffffff",
  },
  {
    id: 85,
    product: "Consumer Non-Durables",
    image: "http://dummyimage.com/215x100.png/5fa2dd/ffffff",
  },
  {
    id: 86,
    product: "Consumer Durables",
    image: "http://dummyimage.com/150x100.png/dddddd/000000",
  },
  {
    id: 87,
    product: "Consumer Services",
    image: "http://dummyimage.com/106x100.png/dddddd/000000",
  },
  {
    id: 88,
    product: "Health Care",
    image: "http://dummyimage.com/198x100.png/dddddd/000000",
  },
  {
    id: 89,
    product: "n/a",
    image: "http://dummyimage.com/175x100.png/cc0000/ffffff",
  },
  {
    id: 90,
    product: "Finance",
    image: "http://dummyimage.com/185x100.png/ff4444/ffffff",
  },
  {
    id: 91,
    product: "Technology",
    image: "http://dummyimage.com/204x100.png/5fa2dd/ffffff",
  },
  {
    id: 92,
    product: "Health Care",
    image: "http://dummyimage.com/187x100.png/cc0000/ffffff",
  },
  {
    id: 93,
    product: "Public Utilities",
    image: "http://dummyimage.com/125x100.png/ff4444/ffffff",
  },
  {
    id: 94,
    product: "Technology",
    image: "http://dummyimage.com/135x100.png/dddddd/000000",
  },
  {
    id: 95,
    product: "Capital Goods",
    image: "http://dummyimage.com/217x100.png/ff4444/ffffff",
  },
  {
    id: 96,
    product: "Health Care",
    image: "http://dummyimage.com/174x100.png/ff4444/ffffff",
  },
  {
    id: 97,
    product: "Public Utilities",
    image: "http://dummyimage.com/192x100.png/cc0000/ffffff",
  },
  {
    id: 98,
    product: "Capital Goods",
    image: "http://dummyimage.com/209x100.png/ff4444/ffffff",
  },
  {
    id: 99,
    product: "Finance",
    image: "http://dummyimage.com/241x100.png/dddddd/000000",
  },
  {
    id: 100,
    product: "Basic Industries",
    image: "http://dummyimage.com/220x100.png/5fa2dd/ffffff",
  },
];
