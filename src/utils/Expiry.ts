/**
 * Easily set future time expiries using human a readable time length
 *
 * @param metric - human readable length, of interval + time parameter
 * @param timestamp - (optional) set expiry from timestamp, defaults to Date.now()
 * @example
 * Time parameters:
 * setExpiry('1y') // 1 year
 * setExpiry('3w') // 3 weeks
 * setExpiry('2d') // 2 days
 * setExpiry('1h') // 1 hour
 * setExpiry('20m') // 20 minutes
 * setExpiry('10s') // 10 seconds
 * @returns
 */
export const setExpiry = (metric: string, timestamp?: number) => {
  const ts = timestamp || Math.floor(Date.now() / 1000);
  const interval = Number(metric.substring(0, metric.length - 1));
  const time = metric.substring(metric.length - 1);
  if (!interval)
    throw new Error("Metric's interval should be equal or greater than 1");

  switch (time) {
    case "s":
      return ts + Math.floor(interval);
    case "m":
      return ts + Math.floor(interval * 60);
    case "h":
      return ts + Math.floor(interval * 3600);
    case "d":
      return ts + Math.floor(interval * 86400);
    case "w":
      return ts + Math.floor(interval * (86400 * 7));
    case "y":
      return ts + Math.floor(interval * 31557600);
    default:
      throw new Error(
        "Missing time parameter: 's' | 'm' | 'h' | 'd' | 'w' | 'y' "
      );
  }
};

export const isExpired = (exp: number) => {
  return exp < Math.floor(Date.now() / 1000);
};
