import React from "react";
import {
  AiOutlineShoppingCart,
  AiFillAccountBook,
  AiFillSkin,
  AiFillHighlight,
  AiTwotoneCrown,
} from "react-icons/ai";
import { FaGlasses } from "react-icons/fa";
import { GiTentacleHeart } from "react-icons/gi";
import { SiRedux, SiDigikeyelectronics } from "react-icons/si";
interface Link {
  id: number;
  text: string;
  path: string;
  icon?: JSX.Element;
}

export const linksNav: Array<Link> = [
  { id: 1, text: "Products", path: "products", icon: <AiFillAccountBook /> },
  { id: 2, text: "Men", path: "men", icon: <AiFillHighlight /> },
  {
    id: 3,
    text: "Electronics",
    path: "electronic",
    icon: <SiDigikeyelectronics />,
  },
  { id: 4, text: "Women", path: "women", icon: <AiFillSkin /> },
  { id: 5, text: "Jewelery", path: "jewelery", icon: <AiTwotoneCrown /> },
  { id: 6, text: "Search", path: "search", icon: <FaGlasses /> },
  { id: 7, text: "Redux with typescript", path: "redux", icon: <SiRedux /> },
  { id: 8, text: "Cart", path: "cart", icon: <AiOutlineShoppingCart /> },

  { id: 9, text: "WishList", path: "wishlist", icon: <GiTentacleHeart /> },
];

interface Input {
  id: number;
  name: string | Category;
  type: string;
  value?: string | number | Category;
}

export enum Category {
  MEN = "men's clothing",
  JEWELEY = "jewelery",
  ELECTRONICS = "electronics",
  WOMEN = "women's clothing",
}

export const inputValues: Array<Input> = [
  { id: 1, name: "title", type: "text", value: "" },
  {
    id: 2,
    name: "category",
    type: "text",
    value: "",
  },
  { id: 3, name: "description", type: "text", value: "" },
  { id: 4, name: "image", type: "text", value: "" },
  { id: 5, name: "price", type: "text", value: "" },
];
