import {
  ActionType,
  AlertApp,
  AlertStatus,
  Product,
  Products,
} from "../types/userAppTypes";
import {
  addNewSessionLocal,
  addUToLocalStorage,
  getUserFromLocalStorage,
  removeUserFromLocalStorage,
} from "../../utils/localStorage";
import { isExpired } from "../../utils/Expiry";
import { ObjectSession, Categories } from "../types/userAppTypes";
import { AppActions } from "../reducers";
import { clienteAxios } from "../../utils/clientAxios";
import { DispatchApp, GetStateApp } from "../store";
import { SESION_LOCAL } from "../initialState";

const sesionAuth = (state: boolean): AppActions => ({
  type: ActionType.LOGAUTH,
  payload: state,
});

const addSessionLocal = (object: ObjectSession): AppActions => ({
  type: ActionType.USER_IN_LOCAL,
  payload: object,
});

export const logoutSession = (): ((dispatch: DispatchApp) => void) => {
  return (dispatch: DispatchApp) => {
    removeUserFromLocalStorage("session");
    dispatch(logOut());
  };
};
const logOut = (): AppActions => ({
  type: ActionType.USER_LOGOUT,
});

export const logInSession = (): ((dispatch: DispatchApp) => void) => {
  return (dispatch: DispatchApp) => {
    dispatch(sesionAuth(true));

    addUToLocalStorage("session", SESION_LOCAL);
  };
};
export const initApp = () => {
  return (dispatch: DispatchApp) => {
    const sessionInLocal = getUserFromLocalStorage("session");
    if (sessionInLocal?.isAuthorized) dispatch(addSessionLocal(sessionInLocal));
    if (sessionInLocal?.exp) {
      const hasExpired = isExpired(sessionInLocal.exp);
      if (hasExpired) {
        removeUserFromLocalStorage("session");
        dispatch(logOut());
      }
    }
  };
};

export const getProducts = () => {
  return async (dispatch: DispatchApp) => {
    dispatch(appStatus({ msg: "", status: AlertStatus.LOADING }));

    try {
      const { data } = await clienteAxios.get("/products");

      dispatch(addProductsStore(data));

      const mens: Products = [];
      const women: Products = [];
      const jewelery: Products = [];
      const electronics: Products = [];

      data.forEach((product: Product) => {
        if (product.category === "men's clothing") mens.push(product);
        if (product.category === "jewelery") jewelery.push(product);
        if (product.category === "electronics") electronics.push(product);
        if (product.category === "women's clothing") women.push(product);
      });
      const categoriesObject = {
        mens: mens,
        women: women,
        jewelery: jewelery,
        electronics: electronics,
      };
      dispatch(addCategoriesStore(categoriesObject));
      dispatch(appStatus({ msg: "", status: AlertStatus.SUCCESS }));
    } catch (error) {
      dispatch(
        appStatus({ msg: "error api products", status: AlertStatus.ERROR })
      );
    }
  };
};

export const appStatus = (object: AlertApp): AppActions => ({
  type: ActionType.SET_APP_STATUS,
  payload: object,
});

const addProductsStore = (object: Products): AppActions => ({
  type: ActionType.ADD_PRODUCTS,
  payload: object,
});

const addCategoriesStore = (object: Categories): AppActions => ({
  type: ActionType.ADD_CATEGORIES,
  payload: object,
});

export const addCartProduct = (id: Array<number>) => {
  return (dispatch: DispatchApp, getState: GetStateApp) => {
    const { exp } = getUserFromLocalStorage("session");
    const session = getState().app.session;

    dispatch(addCart(id));
    session &&
      addUToLocalStorage("session", {
        ...session,
        exp: exp,
        myProfile: {
          ...session.myProfile,
          cart: [...session.myProfile.cart, ...id],
        },
      });
  };
};
const addCart = (id: Array<number>): AppActions => ({
  type: ActionType.ADD_TO_CART,
  payload: id,
});

export const addWishList = (id: Array<number>) => {
  return (dispatch: DispatchApp, getState: GetStateApp) => {
    const session = getState().app.session;
    const { exp } = getUserFromLocalStorage("session");
    dispatch(addWish(id));

    addUToLocalStorage("session", {
      ...session,
      exp: exp,
      myProfile: {
        ...session.myProfile,
        wishList: [...session.myProfile.wishList, ...id],
      },
    });
  };
};
const addWish = (id: Array<number>): AppActions => ({
  type: ActionType.ADD_TO_WISH,
  payload: id,
});

export const UpdateProducts = (newCart: Array<number>, name: string) => {
  return (dispatch: DispatchApp, getState: GetStateApp) => {
    const session = getState().app.session;
    const { exp } = getUserFromLocalStorage("session");
    dispatch(update(newCart, name));

    addNewSessionLocal(session, exp, name, newCart);
  };
};
const update = (newCart: Array<number>, name: string): AppActions => ({
  type: ActionType.UPDATE,
  payload: { update: newCart, name: name },
});
