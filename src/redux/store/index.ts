import { applyMiddleware, createStore, Dispatch } from "redux";
import { rootReducer, AppState, AppActions } from "../reducers";
import thunk, { ThunkMiddleware } from "redux-thunk";
import { createLogger } from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

const logger = createLogger();
const composeEnhancers = composeWithDevTools({ trace: false, traceLimit: 25 });

const initialStore = () => {
  return createStore<AppState, AppActions, {}, {}>(
    rootReducer,

    composeEnhancers(
      applyMiddleware(thunk as ThunkMiddleware<AppState, AppActions>, logger)
    )
  );
};

export const store = initialStore();
export type DispatchApp = Dispatch<AppActions>;
export type GetStateApp = () => AppState;
