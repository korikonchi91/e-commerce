import { ObjectSession } from "./types/userAppTypes";
import { setExpiry } from "../utils/Expiry";
export const SESION_LOCAL: ObjectSession = {
  name: "",
  isAdmin: true,
  avatar: "",
  isAuthorized: true,
  exp: setExpiry("3d"),
  myProfile: {
    cart: [],
    favorites: [],
    wishList: [],
  },
  products: [],
  categories: {
    mens: [],
    women: [],
    jewelery: [],
    electronics: [],
  },
};
