import React from "react";
import { bindActionCreators } from "redux";
import { store } from "../store";
import {
  logInSession,
  getProducts,
  addCartProduct,
  initApp,
  addWishList,
  UpdateProducts,
  logoutSession,
  appStatus,
} from "../actions";

export const useDispatcher = () => {
  return bindActionCreators(
    {
      logInSession,
      initApp,
      getProducts,
      addCartProduct,
      addWishList,
      UpdateProducts,
      logoutSession,
      appStatus,
    },
    store.dispatch
  );
};
