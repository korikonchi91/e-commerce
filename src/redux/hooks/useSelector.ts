import React from "react";
import { TypedUseSelectorHook } from "react-redux";
import { AppState } from "../reducers";
import { useSelector as selector } from "react-redux";

export const useSelector: TypedUseSelectorHook<AppState> = selector;
