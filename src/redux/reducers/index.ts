import { combineReducers } from "redux";
import { nameActions } from "../types/nameActions";
import { nameReducer } from "./nameReducer";
import { userAppActions } from "../types/userAppTypes";
import { AppReducer } from "./AppReducer";
export const rootReducer = combineReducers({
  name: nameReducer,
  app: AppReducer,
});

export type AppActions = userAppActions | nameActions;

export type AppState = ReturnType<typeof rootReducer>;
