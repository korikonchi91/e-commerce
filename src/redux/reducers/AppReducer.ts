import {
  userAppActions,
  ActionType,
  ObjectSession,
  AlertApp,
  UserAppType,
  AlertStatus,
} from "../types/userAppTypes";

export const INIT_SESSION: ObjectSession = {
  name: "",
  isAdmin: true,
  avatar: "",
  isAuthorized: false,
  exp: 0,
  myProfile: {
    cart: [],
    favorites: [],
    wishList: [],
  },
  products: [],
  categories: {
    mens: [],
    women: [],
    jewelery: [],
    electronics: [],
  },
};
const ALERT: AlertApp = {
  msg: "",
  status: AlertStatus.IDLE,
};
const INITIAL_STATE: UserAppType = {
  session: INIT_SESSION,
  alert: ALERT,
};

export const AppReducer = (
  state: UserAppType = INITIAL_STATE,
  action: userAppActions
): UserAppType => {
  switch (action.type) {
    case ActionType.LOGAUTH:
      return {
        ...state,
        session: { ...state.session, isAuthorized: action.payload },
      };
    case ActionType.USER_IN_LOCAL:
      return {
        ...state,
        session: action.payload,
      };
    case ActionType.USER_LOGOUT:
      return {
        ...state,
        session: INIT_SESSION,
      };

    case ActionType.SET_APP_STATUS:
      return {
        ...state,
        alert: {
          ...state.alert,
          msg: action.payload?.msg,
          status: action.payload.status,
        },
      };
    case ActionType.ADD_PRODUCTS:
      return {
        ...state,
        session: { ...state.session, products: action.payload },
      };
    case ActionType.ADD_CATEGORIES:
      return {
        ...state,
        session: { ...state.session, categories: action.payload },
      };
    case ActionType.ADD_TO_CART:
      return {
        ...state,
        session: {
          ...state.session,
          myProfile: {
            ...state.session.myProfile,
            cart: [...state.session.myProfile.cart, ...action.payload],
          },
        },
      };

    case ActionType.ADD_TO_WISH:
      return {
        ...state,
        session: {
          ...state.session,
          myProfile: {
            ...state.session.myProfile,
            wishList: [...state.session.myProfile.wishList, ...action.payload],
          },
        },
      };
    case ActionType.UPDATE:
      return {
        ...state,
        session: {
          ...state.session,
          myProfile: {
            ...state.session.myProfile,
            [action.payload.name]: [...action.payload.update],
          },
        },
      };
    default:
      return state;
  }
};
