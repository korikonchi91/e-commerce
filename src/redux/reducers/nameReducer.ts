import { nameActions } from "../types/nameActions";

export type NameStateType = {
  name: string;
};

const initialState: NameStateType = {
  name: "",
};

export const nameReducer = (
  state: NameStateType = initialState,
  action: nameActions
) => {
  switch (action.type) {
    case "SET_NAME":
      return {
        ...state,
        name: action.payload,
      };
    default:
      return state;
  }
};
