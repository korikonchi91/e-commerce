export enum ActionType {
  LOGAUTH = "LOGAUTH",
  USER_IN_LOCAL = "USER_IN_LOCAL",
  USER_LOGOUT = "USER_LOGOUT",
  SET_APP_STATUS = "SET_APP_STATUS",
  ADD_CATEGORIES = "ADD_CATEGORIES",
  ADD_PRODUCTS = "ADD_PRODUCTS",
  ADD_TO_CART = "ADD_TO_CART",
  ADD_TO_WISH = "ADD_TO_WISH",
  ADD_NEW_PRODUCT = "ADD_NEW_PRODUCT",
  UPDATE = "UPDATE",
}
export type Product = {
  title: string;
  category: string;
  description: string;
  id: number;
  image: string;
  price: number | string;
};
export type Products = Array<Product>;
export interface Categories {
  mens: Products;
  women: Products;
  jewelery: Products;
  electronics: Products;
}
export interface ObjectSession {
  name: string;
  isAdmin: boolean;
  isAuthorized: boolean;
  exp: number;
  avatar: string;
  myProfile: {
    cart: Array<number>;
    favorites: Products;
    wishList: Array<number>;
  };
  products: Products;
  categories: Categories;
}
export type UserAppType = {
  session: ObjectSession;
  alert: AlertApp;
};

export enum AlertStatus {
  IDLE = "IDLE",
  SUCCESS = "SUCCESS",
  ERROR = "ERROR",
  LOADING = "LOADING",
}
export interface AlertApp {
  msg: string;
  status: AlertStatus;
}
export type UserAuth = {
  type: ActionType.LOGAUTH;
  payload: boolean;
};

export type UserLocal = {
  type: ActionType.USER_IN_LOCAL;
  payload: ObjectSession;
};

export type userLogout = {
  type: ActionType.USER_LOGOUT;
};
export type setStatusApp = {
  type: ActionType.SET_APP_STATUS;
  payload: AlertApp;
};

export type addProducts = {
  type: ActionType.ADD_PRODUCTS;
  payload: Products;
};
export type addProductsCategories = {
  type: ActionType.ADD_CATEGORIES;
  payload: Categories;
};

export type addCart = {
  type: ActionType.ADD_TO_CART;
  payload: Array<number>;
};

export type addWish = {
  type: ActionType.ADD_TO_WISH;
  payload: Array<number>;
};
export type UpdateProfile = {
  type: ActionType.UPDATE;
  payload: { update: Array<number>; name: string };
};

export type userAppActions =
  | UserAuth
  | UserLocal
  | userLogout
  | setStatusApp
  | addProducts
  | addProductsCategories
  | addCart
  | addWish
  | UpdateProfile;
